import math
import numpy as np
import os

import sys
sys.path.append('E:\\users\\cl\\anaconda3\\envs\\python36\\lib\\site-packages')

import cv2 as cv
path = 'E:\work\ctpn_preprocess\\img_in'
gt_path = 'E:\work\ctpn_preprocess\\gt_in'

files = os.listdir(path)

for file in files:
	# img file
	filename = os.path.split(file)[1]
	name, type = filename.split('.')
	
	# gt file
	gt_file = os.path.join(gt_path, name + '.txt')
	
	# read img
	img_path = os.path.join(path, file)
	img = cv.imread(img_path)
	height, width, channels = img.shape
	
	# print(height, width, channels)
	origin = (int(width/2), int(height/2))
	
	with open(gt_file, "r") as f:
		lines = f.readlines()

	for line in lines:
		splitted_line = line.strip().lower().split(',')
		
		out_list = []
		tag = splitted_line[8]
		cv.imwrite('img_out\\' + name + '.png', img)
		
		# filter
		if(tag != 'sign'):
			for i in range(0, 7, 2):
		#         print(splitted_line[i] + ', ' + splitted_line[i + 1])
				nx = int(splitted_line[i])
				ny = int(splitted_line[i + 1])
				if(nx == 0):
				    nx = 1
				
				out_list.append((nx, ny))
			
			with open('label_out\\' + name + '.txt', "a") as f:
				for i in range(0, len(out_list)):
					for item in out_list[i]:
						f.writelines(str(item))
						f.writelines(",")
				f.writelines(tag)
				f.writelines("\n")
				