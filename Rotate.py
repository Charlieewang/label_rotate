import math
import numpy as np
import os

import sys
sys.path.append('E:\\users\\cl\\anaconda3\\envs\\python36\\lib\\site-packages')

import cv2 as cv
path = 'E:\work\ctpn_preprocess\\img_in'
gt_path = 'E:\work\ctpn_preprocess\\gt_in'

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.
    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
	
    new_center_x = int (height/2)
    new_center_y = int (width/2)
	
    x = round(qx + (new_center_x - ox))
    if(x == 0):
	    x = 1
    
    return x, round(qy + (new_center_y - oy))
	
def rotateImage(image, angle):
	image_center = tuple(np.array(image.shape[1::-1]) / 2)
	rot_mat = cv.getRotationMatrix2D(image_center, angle, 1.0)
	result = cv.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv.INTER_LINEAR)
	return result

files = os.listdir(path)

# total = 0

for file in files:
	# img file
	filename = os.path.split(file)[1]
	name, type = filename.split('.')
	
	# gt file
	gt_file = os.path.join(gt_path, name + '.txt')
	
	# read img
	img_path = os.path.join(path, file)
	img = cv.imread(img_path)
	height, width, channels = img.shape
	# print(height, width, channels)
	origin = (int(width/2), int(height/2))
	
	with open(gt_file, "r") as f:
		lines = f.readlines()

	for line in lines:
		splitted_line = line.strip().lower().split(',')
		# print(splitted_line)
		
		rotated_list = []
		tag = splitted_line[8]
		
		# rotate img
		# rotatedImg = rotateImage(img, -90)
		rotatedImg = cv.transpose(img)
		rotatedImg = cv.flip(rotatedImg, 1)
		cv.imwrite('img_out\\r_' + name + '.png', rotatedImg)
		
		# filter
		if(tag != 'sign'):
			for i in range(0, 7, 2):
		#         print(splitted_line[i] + ', ' + splitted_line[i + 1])
				nx = int(splitted_line[i])
				ny = int(splitted_line[i + 1])
				
				rotated_list.append(rotate(origin, (nx, ny), math.radians(90)))
			
			with open('label_out\\r_' + name + '.txt', "a") as f:
				for i in range(0, len(rotated_list)):
					for item in rotated_list[i]:
						f.writelines(str(item))
						f.writelines(",")
				f.writelines(tag)
				f.writelines("\n")
				

	# from matplotlib import pyplot as plt
	# show_img = cv.cvtColor(img, cv.COLOR_BGR2RGB) 
	# plt.imshow(show_img)
	# plt.show()

	# rotatedImg = rotateImage(img, -90)

	# show_img = cv.cvtColor(rotatedImg, cv.COLOR_BGR2RGB) 
	# plt.imshow(show_img)
	# plt.show()

	# cv.imwrite("out.png", rotatedImg)